create database mydb;
use mydb;

create table Bairro(
    Nome varchar(50),
    Latitude numeric(8,6),
    Longitude numeric(9,6),
    primary key (Nome)
);
 
create table Escola(
    ID int,
    Logradouro varchar(100),
    Numero int,
    Nome varchar(100),
    IDEB1 numeric(2,1),
    IDEB2 numeric(2,1),
    Bairro_Nome varchar(50),
    Latitude numeric(8,6),
    Longitude numeric(9,6),
primary key (ID),
constraint fk_escola_bairro_nome foreign key (Bairro_Nome) references Bairro(Nome)
    on delete cascade
);  
 
create table Disciplina(
    ID int auto_increment,
    Nome varchar(50),
    Icone longblob,
    primary key (ID)
);
 
create table Escola_Disciplina(
    Disciplina_ID int,
    Escola_ID int,
    Qtd_Professores int,
    primary key(Disciplina_ID, Escola_ID),
    constraint fk_disciplina_id foreign key (Disciplina_ID) references Disciplina(ID)
            on delete cascade,
constraint fk_escola_id foreign key (Escola_ID) references Escola(ID)
    on delete cascade
);
 
create table Ponto_Turistico(
    Nome varchar(100),
    Logradouro varchar(100),
    Numero int,
    Tipo_Ponto_Turistico varchar(50),
    Bairro_Nome varchar(50),
    primary key (Nome),
    constraint fk_pt_bairro_nome foreign key (Bairro_Nome) references Bairro(Nome)
        on delete cascade
);
 
create table Telefone(
    Tel varchar(9),
    Ponto_Turistico_Nome varchar(100),
    primary key (Tel, Ponto_Turistico_Nome),
constraint fk_ponto_turistico_nome foreign key (Ponto_Turistico_Nome) references Ponto_Turistico(Nome)
    on delete cascade
);
var http = require('http'),
	mysql = require('mysql'),
	routes = require('./routes.js');
require('dotenv').config();

var con = mysql.createConnection({
	host: process.env.MYSQL_HOST,
	user: process.env.MYSQL_USER,
	password:process.env.MYSQL_PASSWORD,
	database: process.env.MYSQL_DB
});

con.connect(function(err) {
	if (err) throw err;
	console.log("Connected!");
});

http.createServer(function (req, res) {
	routes.handleRequest(req, res, con);
}).listen(8080);

console.log("Listening on port 8080");
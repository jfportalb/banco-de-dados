var url = require('url'),
	fs = require('fs'),
	mu = require('mu2');
var req, res, con;

function renderHtml(path){
	fs.readFile(path, function(err, data) {
		if (err) {
			notFound();
		} else {
			res.write(data, function(err){

				res.end();
			});
		}
	});
};

function renderOnTemplate(path, view) {
	var allData = "";
	mu.compileAndRender(path, view).on('data', function(data){
		allData = allData+data
	}).on('end', function(){
		mu.compileAndRender("template.html", {content: allData}).pipe(res);
	});
};

function renderImgFromDB(){
	res.writeHead(200, {'Content-Type': 'image/png'});
	id = url.parse(req.url, true).query.id;
	if (!id) id = url.parse(req.url, true).query.disc;
	if (!id) id=13
	con.query(`select icone from Disciplina where ID=${id};`, function(err, result){
		if (err) throw err;
		if (result[0]){
			res.write(result[0].icone);
			res.end();
		} 
		else
			notFound();
	});
}

function notFound(){
	res.writeHead(404, {'Content-Type': 'text/html'});
	renderOnTemplate("404.html");
};

function paginaInicial(){
	// renderHtml("index.html", res);
	let countBairro, countEscola, countPT, bairros, locations, medias, PtDisciplina, disciplinas, BairrosEPT, ProfBairro, mediaIdeb;

	con.query(
		`select E.Bairro_Nome as bairro, avg(IDEB1) as MediaIDEB_1, avg(IDEB2) as MediaIDEB_2, pt
		from Escola E
		left join (select count(*) as pt, Bairro_Nome from Ponto_Turistico as PT group by Bairro_Nome) as CPT on E.Bairro_Nome=CPT.Bairro_Nome
		group by  E.Bairro_Nome;`
		, function (err, result) {
			if (err) throw err;
			medias = result
	});
	con.query(
		`select total, avg(IDEB1) as ideb1, avg(IDEB2) as ideb2 from Escola e join (
    	select b.nome as b_nome, count(p.nome) as total from Bairro b left join Ponto_Turistico p on b.nome = p.bairro_nome 
    	group by b.nome) as T
		on e.bairro_nome = b_nome group by total;`
		, function (err, result) {
			if (err) throw err;
			mediaIdeb = result
	});
	disc = url.parse(req.url, true).query.disc;
	if(disc){
		con.query(
			`select Pt, count(*)/total as Esc
			from Disciplina D 
			inner join Escola_Disciplina ED on D.ID=ED.Disciplina_ID 
			inner join Escola E on ED.Escola_ID=E.ID
			join (select count(*) as total, Bairro_Nome from Escola group by Bairro_Nome) AA on E.Bairro_Nome=AA.Bairro_Nome 
			left join (select count(*) as Pt, Bairro_Nome from Ponto_Turistico group by Bairro_Nome) as PT on E.Bairro_Nome=PT.Bairro_Nome
			where D.ID=`+disc+`
			group by E.Bairro_Nome;`
			, function (err, result) {
				if (err) throw err;
				PtDisciplina = result
		});
		con.query(
			`select B.nome as nome, sum(qtd_professores) as qtd_prof
			from Bairro B
			join Escola E on E.Bairro_Nome=B.Nome
			join Escola_Disciplina ED on ED.Escola_ID=E.ID 
			where Disciplina_ID=`+disc+`
			group by E.Bairro_Nome
			order by qtd_prof desc;`
			, function (err, result) {
				if (err) throw err;
				ProfBairro = result
		});
	}
	searchBairro = url.parse(req.url, true).query.searchBairro;
	if (!searchBairro) searchBairro="";
	con.query("select nome from Bairro where Nome like '%"+searchBairro+"%';", function (err, result) {
		if (err) throw err;
		bairros = result;
	})
	con.query("select distinct Bairro_Nome as nome from Escola where Bairro_Nome in (select distinct Bairro_Nome from Ponto_Turistico);", function (err, result) {
		if (err) throw err;
		BairrosEPT = result;
	})
	con.query("select nome, latitude as lat, longitude as lng from Bairro where nome like '%"+searchBairro+"%';", function (err, result) {
		if (err) throw err;
		locations = result;
	})
	con.query("select nome, id from Disciplina;", function (err, result) {
		if (err) throw err;
		disciplinas = result;
	})
	con.query("select count(*) as count from Bairro;", function (err, result) {
		if (err) throw err;
		countBairro = result[0].count;
	});
	con.query("select count(*) as count from Escola;", function (err, result) {
		if (err) throw err;
		countEscola = result[0].count;
	});
	con.query("select count(*) as count from Ponto_Turistico;", function (err, result) {
		if (err) throw err;
		countPT = result[0].count;
	}).on('end', function(){
		renderOnTemplate("index.html", {mediaIdeb, countBairro,countEscola,countPT, bairros, locations, medias, disciplinas, disc, PtDisciplina, BairrosEPT, ProfBairro});		
	});
};

function bairro(){
	let bairro = url.parse(req.url, true).query.bairro;
	let pts, locations, countPT, countEscola, countBairro, Prof;
	con.query(`select nome, tel 
	  from Ponto_Turistico left join Telefone on Telefone.Ponto_Turistico_Nome = Ponto_Turistico.Nome
	  where Ponto_Turistico.Bairro_Nome='` + bairro + "' order by nome asc;", function(err, result){
			if (err) throw err;
			pts = result
	});
	con.query("select nome, latitude as lat, longitude as lng from Escola where Bairro_Nome='"+bairro+
		"' and latitude is not null and longitude is not null;", function (err, result) {
		if (err) throw err;
		locations = result;
	});
	con.query(`select min,  max, media, 100*media/max as mediaMax, 100*min/max as minMax from
		(select min(qtd_professores) min, max(qtd_professores) max, avg(qtd_professores) media from
		(select sum(qtd_professores) as qtd_professores from Escola E left join Escola_Disciplina ED on E.ID=ED.Escola_ID where Bairro_Nome='`+bairro+"' group by ID) A) B;", function (err, result) {
		if (err) throw err;
		Prof = result[0];
	});
	con.query("select sum(qtd_professores) as countP from Escola join Escola_Disciplina on ID=Escola_ID where Bairro_Nome='"+bairro+"';", function (err, result) {
		if (err) throw err;
		countProfessor = result[0].countP;
	});
	con.query("select count(*) as count from Escola where Bairro_Nome='"+bairro+"';", function (err, result) {
		if (err) throw err;
		countEscola = result[0].count;
	});
	con.query("select count(*) as count from Ponto_Turistico where Bairro_Nome='"+bairro+"';", function (err, result) {
		if (err) throw err;
		countPT = result[0].count;
	}).on('end', function(){
		renderOnTemplate("bairro.html", {bairro, countEscola, countPT, countProfessor, locations, pts, Prof});		
	});
}

module.exports.handleRequest = function(request, response, sqlConnection){
	//Retirar para apresentar
	mu.clearCache();
	
	req = request; res = response; con = sqlConnection;
	res.writeHead(200, {'Content-Type': 'text/html'});
	path = url.parse(req.url, true).pathname;

	switch (path){
		case '/': paginaInicial(); break;
		case '/bairro': bairro(); break;
		case '/disciplinaIcone': renderImgFromDB(); break;
		default: //Arquivos normais ou 404
			filePath = path.substring(1);
			fileType = path.split('.').pop();
			switch(fileType){
				case 'js':
					res.writeHead(200, {'Content-Type': 'text/javascript'});
					renderHtml(filePath, res);
					break;
				case 'jpg':
					res.writeHead(200, {'Content-Type': 'image/jpeg'});
					renderHtml(filePath);
					break;
				default:
					notFound();
			}
	}
};
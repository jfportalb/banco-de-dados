# Análise da Correlação entre Desempenho Escolar e Pontos Culturais em Regiões do Rio de Janeiro
Integrantes:
-João Felipe Porto de Albuquerque 	DRE: 115.038.263
-Rafael Fontella Katopodis 			DRE: 115.021.282
-Silvia Pimpão Vasquez 				DRE: 115.094.560
-Vinícius Garcia Silva da Costa 	DRE: 115.039.031

## Para rodar a aplicação web:
Dentro da pasta do projeto execute:
```sh
cp .env.example .env

# Altere os dados do .env caso seja necessario
# Obs:o campo MYSQL_DB no .env deve ser igual ao banco de dados especificado no arquivo create.sql

npm install;
mysql -u your_user -p < create.sql;
node seed.js;
node server.js;
```
 Agora a aplicação está rodando em localhost:8080
